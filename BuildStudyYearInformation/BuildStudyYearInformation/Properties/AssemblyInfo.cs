using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;


[assembly: AssemblyTitle("BuildStudyYearInformation")]
[assembly: AssemblyDescription("Builds base database records and directory structures for new company and sites in CPA study")]
[assembly: AssemblyProduct("BuildStudyYearInformation")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Solomon Associates")]
[assembly: AssemblyCopyright("Copyright © 2008, Solomon Associates")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: GuidAttribute("77126a15-b124-4186-a652-f24c935d137f")]
[assembly: ComVisibleAttribute(false)]

[assembly: AssemblyVersion("10.0.0.0")]
[assembly: AssemblyFileVersion("10.7.23.1")]
