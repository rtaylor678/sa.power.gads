﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Data.Linq;
using System.Data.Linq.Mapping;


namespace PowerCPA
{
    static class Queries
    {
        // "SELECT * FROM dbo.TSort WHERE (SiteID = 'xxxSiteID')";
        public static Func<PowerWorkDataContext, string, IQueryable<TSort>>
            GetTSortBySiteID =
            CompiledQuery.Compile((PowerWorkDataContext db, string siteid) =>
                from dt in db.TSorts
                where dt.SiteID == siteid
                select dt);

        // "SELECT * FROM dbo.TSort WHERE (Refnum = 'xxxRefnum')";
        public static Func<PowerWorkDataContext, string, IQueryable<TSort>>
            GetTSortByRefnum =
            CompiledQuery.Compile((PowerWorkDataContext db, string refnum) =>
                from dt in db.TSorts
                where dt.Refnum == refnum
                select dt);

        // "SELECT * FROM dbo.StudySites WHERE (SiteID = 'xxxSiteID')";
        public static Func<PowerWorkDataContext, string, IQueryable<StudySite>>
            GetStudySitesBySiteID =
            CompiledQuery.Compile((PowerWorkDataContext db, string siteid) =>
                from dt in db.StudySites
                where dt.SiteID == siteid
                select dt);

        // "SELECT * FROM dbo.ClientInfo WHERE (SiteID = 'xxxSiteID')";
        public static Func<PowerWorkDataContext, string, IQueryable<ClientInfo>>
            GetClientInfoBySiteID =
            CompiledQuery.Compile((PowerWorkDataContext db, string siteid) =>
                from dt in db.ClientInfos
                where dt.SiteID == siteid
                select dt);

        // "SELECT * FROM dbo.NERCFactors WHERE (Refnum = 'xxxRefnum')";
        public static Func<PowerWorkDataContext, string, IQueryable<NERCFactor>>
            GetNERCFactorsByRefnum =
            CompiledQuery.Compile((PowerWorkDataContext db, string refnum) =>
                from dt in db.NERCFactors
                where dt.Refnum == refnum
                select dt);

        // "SELECT * FROM dbo.NERCTurbine WHERE (Refnum = 'xxxRefnum')";
        public static Func<PowerWorkDataContext, string, IQueryable<NERCTurbine>>
            GetNERCTurbineByRefnum =
            CompiledQuery.Compile((PowerWorkDataContext db, string refnum) =>
                from dt in db.NERCTurbines
                where dt.Refnum == refnum
                select dt);

        // "SELECT * FROM dbo.NERCTurbine WHERE (Refnum = 'xxxRefnum') and (UtilityUnitCode == xxxUtilityUnitCode)";
        public static Func<PowerWorkDataContext, string, string, IQueryable<NERCTurbine>>
            GetNERCTurbineByRefnumUtilityUnitCode =
            CompiledQuery.Compile((PowerWorkDataContext db, string refnum, string utilityunitcode) =>
                from dt in db.NERCTurbines
                where dt.Refnum == refnum && dt.UtilityUnitCode == utilityunitcode
                select dt);

        // "SELECT * FROM dbo.ValStat WHERE (Refnum = 'xxxRefnum')";
        public static Func<PowerWorkDataContext, string, IQueryable<ValStat>>
            GetCheckListItemsByRefnum =
            CompiledQuery.Compile((PowerWorkDataContext db, string refnum) =>
                from dt in db.ValStats
                where dt.Refnum == refnum
                select dt);

        // "SELECT * FROM dbo.ValStatSource";
        public static Func<PowerWorkDataContext, IQueryable<ValStatSource>>
            GetCheckListMasterItems =
            CompiledQuery.Compile((PowerWorkDataContext db) =>
                from dt in db.ValStatSources
                select dt);


        //public static Func<PowerGlobal, string, int, IQueryable<Prices>>
        //    GetPricingHubDetailsByNameYear =
        //    CompiledQuery.Compile((PowerGlobal db, string name, int year) =>
        //        from dt in db.Prices
        //        where dt.PricingHub == name && dt.StartTime.Year == year
        //        select dt);
        //}
        //class SimplePricingHub
        //{
        //    public string PricingHub { get; set; }
        //}
        /*
        class SimpleCustomer
        {
            public string ContactName { get; set; }
        }

        class Queries2
        {
            public static Func<Northwnd, string, IEnumerable<SimpleCustomer>> 
                CustomersByCity =
                CompiledQuery.Compile<Northwnd, string, IEnumerable<SimpleCustomer>>(
                (Northwnd db, string city) =>
                from c in db.Customers
                where c.City == city
                select new SimpleCustomer { ContactName = c.ContactName });
        }

        */
    }

    //[Table(Name = "PriceDetails")]
    public class PriceDetails
    {
        [Column(IsPrimaryKey = true)]
        public string PricingHub { get; set; }
        [Column]
        public DateTime Date { get; set; }
        [Column]
        public decimal OffPeakPrice { get; set; }
        [Column]
        public decimal PeakPrice { get; set; }
        [Column]
        public decimal AvgPeakPrice { get; set; }
    }

}
