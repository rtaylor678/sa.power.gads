using System;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace PowerCPA
{
    internal partial class Globals
    {
        #region enums
        public enum Roles
        {
            Developer = 1000,
            Administrator = 500,
            PowerUser = 400,
            Consultant = 300
        };

        #endregion enums

        #region EventHandlers
        #region OnStatusMessageChanged
        public static event EventHandler StatusMessageChanged;
        /// <summary>
        /// Triggers the StatusMessageChanged event.
        /// </summary>
        public static void OnStatusMessageChanged()
        {
            if (StatusMessageChanged != null)
                StatusMessageChanged(Globals.StatusMessageChanged, new EventArgs());
        }
        #endregion OnStatusMessageChanged
        #endregion EventHandlers

        #region Help Provider
        public HelpProvider helpProvider;

        #endregion Help Provider

        #region Constants
        #region Filenames
        public const string FILENAME_XML_MAIN = "PowerProcessingConsoleSettings.xml";
        #endregion Filenames

        #region Tablenames
        // setup type tables
        public const string TABLE_SETTING = "Setting";

        #endregion Tablenames

        public const int VALID_YEAR_LOWERLIMIT = 2000;
        // These are the Win32 error code for file not found or access denied.
        public const int ERROR_FILE_NOT_FOUND = 2;
        public const int ERROR_ACCESS_DENIED = 5;
        public const string TYPESTEAM = "STEAM";
        public const string TYPECC = "CC";

        public const string SQL_PRICINGHUB_LU = "SELECT RTRIM([PricingHub]) AS 'Name', RTRIM([PricingHub]) + ISNULL(' (' +  RTRIM([HubDescription]) + ' )','') AS [Desc]" +
            " FROM [PowerGlobal].[dbo].[PricingHub_LU]";
        public const string SQL_REGION_LU = "SELECT RTRIM([Region]) AS 'Name',RTRIM([Region]) + ISNULL(' (' +  RTRIM([Desc]) + ' )','') AS [Desc]" +
            " FROM [PowerGlobal].[dbo].[Region_LU]";
        public const string SQL_TURBINETYPE_LU = "SELECT RTRIM([Name]) AS 'Name',RTRIM([Name]) + ISNULL(' (' +  RTRIM([Desc]) + ' )','') AS [Desc]" +
            " FROM [PowerGlobal].[dbo].[TurbineType_LU]";
        public const string SQL_EGCREGIONS_LU = "SELECT RTRIM([RegionName]) AS 'Name',RTRIM([RegionName]) + ISNULL(' (' +  RTRIM([Description]) + ' )','') AS [Desc]" +
            " FROM [PowerGlobal].[dbo].[EGCRegions]";
        public const string SQL_EGCTECHNOLOGY_LU = "SELECT RTRIM([Technology]) AS 'Name',RTRIM([Technology]) + ISNULL(' (' +  RTRIM([Description]) + ' )','') AS [Desc]" +
            " FROM [PowerGlobal].[dbo].[Technology_LU]";
        public const string SQL_TSORTFUELTYPE_LU = "SELECT RTRIM([Type]) AS 'Name',RTRIM([Type])  + ISNULL(' (' +  RTRIM([Desc]) + ' )','') AS [Desc]" +
            " FROM [PowerGlobal].[dbo].[TSortFuelType_LU]";
        public const string SQL_CONTINENT_LU = "SELECT RTRIM([Continent]) AS 'Name', RTRIM([Continent]) AS [Desc]" +
            " FROM [PowerGlobal].[dbo].[Continent_LU]";
        public const string SQL_COUNTRY_LU = "SELECT RTRIM([Country]) AS 'Name',RTRIM([Country])+ ISNULL(' (' +  RTRIM([Abbrev2ch]) + ' )','') AS [Desc]" +
            " FROM [PowerGlobal].[dbo].[Country_LU]";
        //public const string SQL_ = "" +
        //    " ";

        #region encryption keys
        public static readonly string EncryptionKey = "F3l1u4e1g5g9e2G6r5o3u5p9";
        public static readonly string EncryptionIV = "3F1l4u1e5g9g2e6G5r3o5u9p";
        public static readonly string EncryptSelect = "S@Se1ect";
        public static readonly string EncryptExec = "S@3xec";
        #endregion encryption keys

        #endregion Constants

        #region Global Variables
        public static SqlConnectionStringBuilder sqlConnPW = new SqlConnectionStringBuilder();
        public static SqlConnectionStringBuilder sqlConnPG = new SqlConnectionStringBuilder();
        public static String connString = String.Empty;

        public static bool bFormCancel;
        public static bool bInitialLoad = true;
        public static bool bRunningLocal;
        public static List<string> Developers = new List<string>();
        public static List<string> Administrators = new List<string>();
        public static List<string> PowerUsers = new List<string>();

        public static List<CompanyInfo> CompanyDetails = new List<CompanyInfo>();
        public static List<SiteInfo> SiteDetails = new List<SiteInfo>();
        public static List<UnitInfo> UnitDetails = new List<UnitInfo>();
        public static List<string> BaseDirectoryList = new List<string>();

        public static DataSet dsStudySites = new DataSet("StudySites");
        public static DataSet dsTSort = new DataSet("TSort");
        public static DataSet dsNERCFactors = new DataSet("NERCFactors");
        public static DataSet dsNERCTurbine = new DataSet("NERCTurbine");
        public static string strSQLGetSiteIDbyCompanyIDSiteName =
            "SELECT top 2 * FROM dbo.StudySites WHERE (CompanyID = 'xxxCompanyID' AND SiteName = 'xxxSiteName') AND StudyYear >= 2000 ORDER BY SiteID DESC";
        public static string strSQLGetRefnumsbySiteID =
            "SELECT * FROM dbo.TSort WHERE (SiteID = 'xxxSiteID')";
        public static DataTable Countries = new DataTable();
        public static DataTable Regions = new DataTable();
        public static DataTable Continents = new DataTable();
        public static DataTable FuelTypes = new DataTable();
        public static DataTable TurbineTypes = new DataTable();
        public static DataTable EGCTechnologies = new DataTable();
        public static DataTable EGCRegions = new DataTable();
        public static DataTable PricingHubs = new DataTable();
        public static List<int> ScrubbersYNs = new List<int>(2);
        public static List<int> PrecBagYNs = new List<int>(2);
        public static List<string> Regulateds = new List<string>(2);
        public static List<string> CalcCommUnavails = new List<string>(2);
   
        //public static List<Row_Company_LU> CompanyRows = new List<Row_Company_LU>();
        //public static List<Row_StudySite> StudySiteRows = new List<Row_StudySite>();
        //public static List<Row_TSort> TSortRows = new List<Row_TSort>();
        //public static List<Row_NercFactor> NercFactorRows = new List<Row_NercFactor>();
        //public static List<Row_NercTurbine> NercTurbineRows = new List<Row_NercTurbine>();

        #endregion Global Variables

        #region Local Variables
        private static string _currentUser = That.User.Name;
        private static string _currentYear;
        private static string _initialDirectory = string.Empty;
        private static string _tempDirectory = string.Empty;
        private static string _StatusMessageText = string.Empty;
        private static string _basePath = string.Empty;

        #endregion Local Variables

        #region Public Properties
        public static string CurrentUser
        {
            get { return (_currentUser); }
            set
            {
                _currentUser = value;
                // now set the UserRole
                if (Administrators.Contains(_currentUser))
                    UserRole = (int)Roles.Administrator;
                else if (Developers.Contains(_currentUser))
                    UserRole = (int)Roles.Developer;
                else if (PowerUsers.Contains(_currentUser))
                    UserRole = (int)Roles.PowerUser;
            }
        }
        #region public static string WindowsUserName

        /// <summary>
        /// Gets or sets the WindowsUserName.
        /// </summary>
        /// <value>The WindowsUserName.</value>
        public static string WindowsUserName
        {
            get;
            set;
        }

        #endregion public string WindowsUserName
        #region public static int UserRole

        /// <summary>
        /// Gets or sets the UserRole.
        /// </summary>
        /// <value>The UserRole.</value>
        public static int UserRole
        {
            get;
            set;
        }

        #endregion
        public static string CurrentYear
        {
            get { return _currentYear; }
            set
            {
                _currentYear = value;
                CurrentYear2Char = _currentYear.Substring(2, 2);
                PriorYear2Char = (Int32.Parse(CurrentYear2Char) - 1).ToString("00");
            }
        }
        public static string CurrentYear2Char { get; private set; }
        public static string PriorYear2Char { get; private set; }
        public static string InitialDirectory
        {
            get { return _initialDirectory; }
            set
            {
                _initialDirectory = value;
                if (!_initialDirectory.EndsWith(@"\"))
                {
                    _initialDirectory += @"\";
                }
            }
        }
        public static string TempDirectory
        {
            get { return _tempDirectory; }
            set
            {
                _tempDirectory = value;
                if (!_tempDirectory.EndsWith(@"\"))
                {
                    _tempDirectory += @"\";
                }
            }
        }
        /// <summary>
        /// Gets or sets the StatusMessageText.
        /// </summary>
        /// <value>The StatusMessageText.</value>
        public static string StatusMessageText
        {
            get
            {
                return _StatusMessageText;
            }
            set
            {
                _StatusMessageText = value;
                Globals.OnStatusMessageChanged();
            }
        }
        public static string BasePath
        {
            get { return _basePath; }
            set
            {
                _basePath = value;
                if (!_basePath.EndsWith(@"\"))
                {
                    _basePath += @"\";
                }
            }
        }
        #region public static bool WorkingOnline

        /// <summary>
        /// Gets or sets the WorkingOnline.
        /// </summary>
        /// <value>The WorkingOnline.</value>
        public static bool WorkingOnline
        {
            get;
            set;
        }

        #endregion
        #endregion Public Properties

        #region Constructors
        internal Globals()
        {
            helpProvider = new HelpProvider();
        }
        #endregion Constructors

        #region Events Raised

        #endregion Events Raised

        #region Public Methods

        #endregion Public Methods

        #region Private Methods

        #endregion Private Methods

    }
}
