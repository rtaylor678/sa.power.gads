﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerCPA
{
    // Delegate declaration.
    public delegate void UnitCodeEventHandler(object sender, UnitCodeEventArgs e);

    // Class that contains the data for 
    // the alarm event. Derives from System.EventArgs.
    public class UnitCodeEventArgs : EventArgs
    {
        private readonly string unitCode;

        //Constructor.
        public UnitCodeEventArgs(string unitCode)
        {
            this.unitCode = unitCode;
        }

        // The SnoozePressed property indicates whether the snooze
        // button is pressed on the alarm when the alarm event is generated.
        public string UnitCode
        {
            get { return unitCode; }
        }
    }


    // Delegate declaration.
    public delegate void UtilityCodeEventHandler(object sender, UtilityCodeEventArgs e);

    // Class that contains the data for 
    // the alarm event. Derives from System.EventArgs.
    public class UtilityCodeEventArgs : EventArgs
    {
        private readonly string utilityCode;

        //Constructor.
        public UtilityCodeEventArgs(string utilityCode)
        {
            this.utilityCode = utilityCode;
        }

        // The SnoozePressed property indicates whether the snooze
        // button is pressed on the alarm when the alarm event is generated.
        public string UtilityCode
        {
            get { return utilityCode; }
        }
    }
}
