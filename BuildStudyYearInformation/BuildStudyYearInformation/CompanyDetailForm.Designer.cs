﻿namespace PowerCPA
{
    partial class CompanyDetailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CompanyDetailForm));
            this.treeViewCompanyDetail = new System.Windows.Forms.TreeView();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.statusBarMessage = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnCreateRecords = new System.Windows.Forms.Button();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.lblTopMessage = new System.Windows.Forms.Label();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.pnlBottom.SuspendLayout();
            this.statusBar.SuspendLayout();
            this.pnlTop.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeViewCompanyDetail
            // 
            this.treeViewCompanyDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeViewCompanyDetail.FullRowSelect = true;
            this.treeViewCompanyDetail.Location = new System.Drawing.Point(0, 0);
            this.treeViewCompanyDetail.Name = "treeViewCompanyDetail";
            this.treeViewCompanyDetail.Size = new System.Drawing.Size(454, 261);
            this.treeViewCompanyDetail.TabIndex = 0;
            
            // 
            // pnlBottom
            // 
            this.pnlBottom.Controls.Add(this.statusBar);
            this.pnlBottom.Controls.Add(this.btnClose);
            this.pnlBottom.Controls.Add(this.btnCreateRecords);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 283);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(454, 52);
            this.pnlBottom.TabIndex = 1;
            // 
            // statusBar
            // 
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusBarMessage});
            this.statusBar.Location = new System.Drawing.Point(0, 30);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(454, 22);
            this.statusBar.TabIndex = 2;
            this.statusBar.Text = "statusStrip1";
            // 
            // statusBarMessage
            // 
            this.statusBarMessage.Name = "statusBarMessage";
            this.statusBarMessage.Size = new System.Drawing.Size(103, 17);
            this.statusBarMessage.Text = "toolStripStatusLabel";
            this.statusBarMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(227, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(105, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.OnClickClose);
            // 
            // btnCreateRecords
            // 
            this.btnCreateRecords.Location = new System.Drawing.Point(122, 4);
            this.btnCreateRecords.Name = "btnCreateRecords";
            this.btnCreateRecords.Size = new System.Drawing.Size(105, 23);
            this.btnCreateRecords.TabIndex = 0;
            this.btnCreateRecords.Text = "Create Records";
            this.btnCreateRecords.UseVisualStyleBackColor = true;
            this.btnCreateRecords.Click += new System.EventHandler(this.OnClickCreateRecords);
            // 
            // pnlTop
            // 
            this.pnlTop.Controls.Add(this.lblTopMessage);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(454, 22);
            this.pnlTop.TabIndex = 2;
            // 
            // lblTopMessage
            // 
            this.lblTopMessage.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTopMessage.Location = new System.Drawing.Point(0, 0);
            this.lblTopMessage.Name = "lblTopMessage";
            this.lblTopMessage.Size = new System.Drawing.Size(454, 20);
            this.lblTopMessage.TabIndex = 3;
            this.lblTopMessage.Text = "Click Create Records to build all of the TSort, NercTurbine, NercFactors & StudyS" +
                "ite records.";
            this.lblTopMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.treeViewCompanyDetail);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 22);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(454, 261);
            this.pnlMain.TabIndex = 3;
            // 
            // CompanyDetailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 335);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.pnlTop);
            this.Controls.Add(this.pnlBottom);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(462, 369);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(462, 369);
            this.Name = "CompanyDetailForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Company Detail";
            this.Load += new System.EventHandler(this.OnLoad);
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.pnlTop.ResumeLayout(false);
            this.pnlMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeViewCompanyDetail;
        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnCreateRecords;
        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Label lblTopMessage;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStripStatusLabel statusBarMessage;
    }
}