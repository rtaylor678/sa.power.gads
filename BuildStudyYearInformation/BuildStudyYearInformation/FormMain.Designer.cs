﻿namespace PowerCPA
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.statusBarStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusBarClock = new System.Windows.Forms.ToolStripStatusLabel();
            this.ClockTimer = new System.Windows.Forms.Timer(this.components);
            this.pnlTop = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.txtOutputYear = new Common.CustomControls.NumericTextBox();
            this.lblOutputYear = new System.Windows.Forms.Label();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnAbout = new System.Windows.Forms.Button();
            this.btnCreateSiteDirs = new System.Windows.Forms.Button();
            this.btnBuildBaseDirs = new System.Windows.Forms.Button();
            this.btnCreateCompanyDirs = new System.Windows.Forms.Button();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.splitContainerMain = new System.Windows.Forms.SplitContainer();
            this.splitContainerLeft = new System.Windows.Forms.SplitContainer();
            this.listViewSource = new System.Windows.Forms.ListView();
            this.listViewCompanyNames = new System.Windows.Forms.ListView();
            this.listViewOutputDir = new System.Windows.Forms.ListView();
            this.statusBar.SuspendLayout();
            this.pnlTop.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.splitContainerMain.Panel1.SuspendLayout();
            this.splitContainerMain.Panel2.SuspendLayout();
            this.splitContainerMain.SuspendLayout();
            this.splitContainerLeft.Panel1.SuspendLayout();
            this.splitContainerLeft.Panel2.SuspendLayout();
            this.splitContainerLeft.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusBar
            // 
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusBarStatus,
            this.statusBarClock});
            this.statusBar.Location = new System.Drawing.Point(0, 504);
            this.statusBar.MinimumSize = new System.Drawing.Size(200, 0);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(762, 22);
            this.statusBar.TabIndex = 18;
            // 
            // statusBarStatus
            // 
            this.statusBarStatus.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.statusBarStatus.Name = "statusBarStatus";
            this.statusBarStatus.Size = new System.Drawing.Size(667, 17);
            this.statusBarStatus.Spring = true;
            this.statusBarStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // statusBarClock
            // 
            this.statusBarClock.AutoSize = false;
            this.statusBarClock.Name = "statusBarClock";
            this.statusBarClock.Size = new System.Drawing.Size(80, 17);
            // 
            // pnlTop
            // 
            this.pnlTop.Controls.Add(this.button1);
            this.pnlTop.Controls.Add(this.txtOutputYear);
            this.pnlTop.Controls.Add(this.lblOutputYear);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(762, 32);
            this.pnlTop.TabIndex = 19;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(591, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtOutputYear
            // 
            this.txtOutputYear.Location = new System.Drawing.Point(386, 6);
            this.txtOutputYear.MaxLength = 4;
            this.txtOutputYear.Name = "txtOutputYear";
            this.txtOutputYear.Size = new System.Drawing.Size(58, 20);
            this.txtOutputYear.TabIndex = 0;
            // 
            // lblOutputYear
            // 
            this.lblOutputYear.AutoSize = true;
            this.lblOutputYear.Location = new System.Drawing.Point(319, 10);
            this.lblOutputYear.Name = "lblOutputYear";
            this.lblOutputYear.Size = new System.Drawing.Size(67, 13);
            this.lblOutputYear.TabIndex = 3;
            this.lblOutputYear.Text = "Output Year:";
            // 
            // pnlBottom
            // 
            this.pnlBottom.Controls.Add(this.btnExit);
            this.pnlBottom.Controls.Add(this.btnAbout);
            this.pnlBottom.Controls.Add(this.btnCreateSiteDirs);
            this.pnlBottom.Controls.Add(this.btnBuildBaseDirs);
            this.pnlBottom.Controls.Add(this.btnCreateCompanyDirs);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottom.Location = new System.Drawing.Point(0, 476);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(762, 28);
            this.pnlBottom.TabIndex = 20;
            // 
            // btnExit
            // 
            this.btnExit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnExit.Location = new System.Drawing.Point(560, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 11;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.OnClickExit);
            // 
            // btnAbout
            // 
            this.btnAbout.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAbout.Location = new System.Drawing.Point(485, 3);
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(75, 23);
            this.btnAbout.TabIndex = 10;
            this.btnAbout.Text = "About";
            this.btnAbout.UseVisualStyleBackColor = true;
            this.btnAbout.Click += new System.EventHandler(this.OnClickAbout);
            // 
            // btnCreateSiteDirs
            // 
            this.btnCreateSiteDirs.Location = new System.Drawing.Point(366, 3);
            this.btnCreateSiteDirs.Name = "btnCreateSiteDirs";
            this.btnCreateSiteDirs.Size = new System.Drawing.Size(119, 23);
            this.btnCreateSiteDirs.TabIndex = 3;
            this.btnCreateSiteDirs.Text = "Create Site Dirs";
            this.btnCreateSiteDirs.UseVisualStyleBackColor = true;
            this.btnCreateSiteDirs.Click += new System.EventHandler(this.OnClickCreateSiteDirs);
            // 
            // btnBuildBaseDirs
            // 
            this.btnBuildBaseDirs.Location = new System.Drawing.Point(128, 3);
            this.btnBuildBaseDirs.Name = "btnBuildBaseDirs";
            this.btnBuildBaseDirs.Size = new System.Drawing.Size(119, 23);
            this.btnBuildBaseDirs.TabIndex = 1;
            this.btnBuildBaseDirs.Text = "Build Base Dirs";
            this.btnBuildBaseDirs.UseVisualStyleBackColor = true;
            this.btnBuildBaseDirs.Click += new System.EventHandler(this.OnClickBuildBaseDirs);
            // 
            // btnCreateCompanyDirs
            // 
            this.btnCreateCompanyDirs.Location = new System.Drawing.Point(247, 3);
            this.btnCreateCompanyDirs.Name = "btnCreateCompanyDirs";
            this.btnCreateCompanyDirs.Size = new System.Drawing.Size(119, 23);
            this.btnCreateCompanyDirs.TabIndex = 0;
            this.btnCreateCompanyDirs.Text = "Create Company Dirs";
            this.btnCreateCompanyDirs.UseVisualStyleBackColor = true;
            this.btnCreateCompanyDirs.Click += new System.EventHandler(this.OnClickCreateCompanyDirs);
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.splitContainerMain);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 32);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(762, 444);
            this.pnlMain.TabIndex = 21;
            // 
            // splitContainerMain
            // 
            this.splitContainerMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerMain.Location = new System.Drawing.Point(0, 0);
            this.splitContainerMain.Name = "splitContainerMain";
            // 
            // splitContainerMain.Panel1
            // 
            this.splitContainerMain.Panel1.Controls.Add(this.splitContainerLeft);
            this.splitContainerMain.Panel1MinSize = 100;
            // 
            // splitContainerMain.Panel2
            // 
            this.splitContainerMain.Panel2.Controls.Add(this.listViewOutputDir);
            this.splitContainerMain.Panel2MinSize = 100;
            this.splitContainerMain.Size = new System.Drawing.Size(762, 444);
            this.splitContainerMain.SplitterDistance = 307;
            this.splitContainerMain.TabIndex = 2;
            // 
            // splitContainerLeft
            // 
            this.splitContainerLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerLeft.Location = new System.Drawing.Point(0, 0);
            this.splitContainerLeft.Name = "splitContainerLeft";
            this.splitContainerLeft.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerLeft.Panel1
            // 
            this.splitContainerLeft.Panel1.Controls.Add(this.listViewSource);
            // 
            // splitContainerLeft.Panel2
            // 
            this.splitContainerLeft.Panel2.Controls.Add(this.listViewCompanyNames);
            this.splitContainerLeft.Size = new System.Drawing.Size(307, 444);
            this.splitContainerLeft.SplitterDistance = 203;
            this.splitContainerLeft.TabIndex = 1;
            // 
            // listViewSource
            // 
            this.listViewSource.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewSource.Location = new System.Drawing.Point(0, 0);
            this.listViewSource.Name = "listViewSource";
            this.listViewSource.Size = new System.Drawing.Size(307, 203);
            this.listViewSource.TabIndex = 1;
            this.listViewSource.UseCompatibleStateImageBehavior = false;
            // 
            // listViewCompanyNames
            // 
            this.listViewCompanyNames.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewCompanyNames.Location = new System.Drawing.Point(0, 0);
            this.listViewCompanyNames.Name = "listViewCompanyNames";
            this.listViewCompanyNames.Size = new System.Drawing.Size(307, 237);
            this.listViewCompanyNames.TabIndex = 1;
            this.listViewCompanyNames.UseCompatibleStateImageBehavior = false;
            this.listViewCompanyNames.SelectedIndexChanged += new System.EventHandler(this.listViewCompanyNames_SelectedIndexChanged);
            this.listViewCompanyNames.DoubleClick += new System.EventHandler(this.OnDoubleClickCompanyNames);
            // 
            // listViewOutputDir
            // 
            this.listViewOutputDir.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewOutputDir.Location = new System.Drawing.Point(0, 0);
            this.listViewOutputDir.Name = "listViewOutputDir";
            this.listViewOutputDir.Size = new System.Drawing.Size(451, 444);
            this.listViewOutputDir.TabIndex = 0;
            this.listViewOutputDir.UseCompatibleStateImageBehavior = false;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(762, 526);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.pnlTop);
            this.Controls.Add(this.statusBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(770, 560);
            this.Name = "FormMain";
            this.Text = "Build Study Year Information";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnClosing);
            this.Load += new System.EventHandler(this.OnLoadMainForm);
            this.Resize += new System.EventHandler(this.OnResizeMainForm);
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            this.pnlBottom.ResumeLayout(false);
            this.pnlMain.ResumeLayout(false);
            this.splitContainerMain.Panel1.ResumeLayout(false);
            this.splitContainerMain.Panel2.ResumeLayout(false);
            this.splitContainerMain.ResumeLayout(false);
            this.splitContainerLeft.Panel1.ResumeLayout(false);
            this.splitContainerLeft.Panel2.ResumeLayout(false);
            this.splitContainerLeft.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStripStatusLabel statusBarStatus;
        private System.Windows.Forms.ToolStripStatusLabel statusBarClock;
        private System.Windows.Forms.Timer ClockTimer;
        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Button btnCreateCompanyDirs;
        private System.Windows.Forms.Panel pnlMain;
        private Common.CustomControls.NumericTextBox txtOutputYear;
        private System.Windows.Forms.Label lblOutputYear;
        private System.Windows.Forms.Button btnBuildBaseDirs;
        private System.Windows.Forms.SplitContainer splitContainerMain;
        private System.Windows.Forms.ListView listViewOutputDir;
        private System.Windows.Forms.SplitContainer splitContainerLeft;
        private System.Windows.Forms.ListView listViewCompanyNames;
        private System.Windows.Forms.Button btnCreateSiteDirs;
        private System.Windows.Forms.ListView listViewSource;
        private System.Windows.Forms.Button btnAbout;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button button1;
    }
}

