﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerCPA
{
    public class SiteInfo
    {
        public string Name { get; set; }
        public string SiteID { get; set; }
        public string PriorSiteID { get; set; }
        public string Parent { get; set; }
    }
}
