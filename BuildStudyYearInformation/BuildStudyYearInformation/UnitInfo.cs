﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerCPA
{
    public class UnitInfo
    {
        public string Name { get; set; }
        public string RefNum { get; set; }
        public string Parent { get; set; }
    }
}
