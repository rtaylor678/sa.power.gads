﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Configuration;

using ComUtilsAlias = Common.Utility;
using ComUIUtilsAlias = Common.UI;
using DBUtilsAlias = Common.Database;

namespace PowerCPA
{
    public partial class FormMain : Form
    {
        #region Variables

        #region Local Variables
        protected bool bInitializing = true;
        protected string initialDirectory = string.Empty;
        protected string tempDirectory = string.Empty;
        protected bool bRunningLocal;
        protected List<string> directories = new List<string>();
        protected List<string> companies = new List<string>();
        protected string subPath = string.Empty;
        protected List<string> ignoreFiles = new List<string>();
        protected string PwrCCMasterFile = "PowerCCXX.xls";
        protected string PwrStmMasterFile = "PowerXX.xls";
        protected string PowerCDDir = string.Empty;
        #endregion Local Variables

        #region Global Variables

        #endregion Global Variables

        #endregion Variables

        #region Constructors
        public FormMain()
        {
            InitializeComponent();
            txtOutputYear.AllowDecimal = false;

            Globals.StatusMessageChanged += StatusMessageTextChanged;

            LoadConfiguration();

            Text = String.Format("{0} ({1})", Text, Globals.DataSource);

         //   Globals.bFormCancel = !Globals.PromptForSQLLogin();
            Globals.bFormCancel = false;

        }
        #endregion Constructors

        #region Form Events
        private void OnLoadMainForm(object sender, EventArgs e)
        {
            //if (Globals.bFormCancel)
            //{
            //    CloseApplication();
            //    return;
            //}
            ////if (Globals.UserRole < (int)Globals.Roles.Administrator)
            ////{
            ////    MessageBox.Show("You are not authorized to run this application");
            ////    Globals.bFormCancel = true;
            ////}

            //if (Globals.bFormCancel)
            //{
            //    MessageBox.Show("Problem logging onto the SQL server, closing app");
            //    CloseApplication();
            //}

            Globals.sqlConnPW = Globals.InitializeSQLConnectionStringBuilder("PowerWork", 90);
            Globals.sqlConnPG = Globals.InitializeSQLConnectionStringBuilder("PowerGlobal", 90);

            btnCreateCompanyDirs.Enabled = false;
            btnCreateSiteDirs.Enabled = false;

            // populate list of basic directories required
            Globals.BaseDirectoryList.Add(@"\Analysis\");
            Globals.BaseDirectoryList.Add(@"\Analysis\Downloads\");
            //Globals.BaseDirectoryList.Add(@"\Aux Metric Issue\");
            Globals.BaseDirectoryList.Add(@"\Company\");
            Globals.BaseDirectoryList.Add(@"\Company\Custom Peer Groups\");
            Globals.BaseDirectoryList.Add(@"\Company Correspondence\");
            Globals.BaseDirectoryList.Add(@"\Correspondence\");
            Globals.BaseDirectoryList.Add(@"\Output diskettes\");
            Globals.BaseDirectoryList.Add(@"\Output diskettes\Validated Input\");
            Globals.BaseDirectoryList.Add(@"\Output diskettes\Working\");
            //Globals.BaseDirectoryList.Add(@"\Pending\");
            Globals.BaseDirectoryList.Add(@"\Plant\");
            Globals.BaseDirectoryList.Add(@"\Plant Names\");
            Globals.BaseDirectoryList.Add(@"\PowerCDs\");
            //Globals.BaseDirectoryList.Add(@"\PowerCDs\CC\");
            //Globals.BaseDirectoryList.Add(@"\PowerCDs\Steam\");
            Globals.BaseDirectoryList.Add(@"\Presentations\");
            //Globals.BaseDirectoryList.Add(@"\Presentations\As Given\");
            //Globals.BaseDirectoryList.Add(@"\Presentations\Data\");
            //Globals.BaseDirectoryList.Add(@"\Presentations\Pricing\");
            Globals.BaseDirectoryList.Add(@"\Presentations\Template\");
            Globals.BaseDirectoryList.Add(@"\Presentations\Template\Data\");
            //Globals.BaseDirectoryList.Add(@"\Presentations\Template\LiveCharts\");
            Globals.BaseDirectoryList.Add(@"\Presentations\Template\PerfSummBars\");
            Globals.BaseDirectoryList.Add(@"\Presentations\Template\Trends\");
            Globals.BaseDirectoryList.Add(@"\Review\");
            Globals.BaseDirectoryList.Add(@"\Review\ROODs\");
            //Globals.BaseDirectoryList.Add(@"\Review\ROODs\Uploaded\");

            listViewSource.View = View.Details;
            listViewSource.Columns.Add("Full Path", listViewSource.ClientRectangle.Width, HorizontalAlignment.Left);
            listViewSource.Columns[0].Name = "FullPath";

            listViewOutputDir.View = View.Details;
            // Add a column with width 20 and left alignment.
            int colWidthFullPath = listViewOutputDir.ClientRectangle.Width - 60;
            listViewOutputDir.Columns.Add("Full Path", colWidthFullPath, HorizontalAlignment.Left);
            listViewOutputDir.Columns[0].Name = "FullPath";
            listViewOutputDir.Columns.Add("Status", 60, HorizontalAlignment.Left);
            listViewOutputDir.Columns[1].Name = "Status";
            listViewOutputDir.Sorting = SortOrder.Ascending;

            listViewCompanyNames.View = View.Details;
            // Add a column with width 20 and left alignment.
            listViewCompanyNames.Columns.Add("Company Names", listViewCompanyNames.ClientRectangle.Width, HorizontalAlignment.Left);
            listViewCompanyNames.Columns[0].Name = "CompanyNames";
            listViewCompanyNames.MultiSelect = false;

            //// test to see if you are inside Solomon firewall or not
            //try
            //{
            //    using (PowerGlobalDataContext dc = new PowerGlobalDataContext(Globals.sqlConnPG.ConnectionString))
            //    {
            //        int test = (from country in dc.Country_LUs
            //                    orderby country.Country
            //                    select country).Count();
            //        Globals.WorkingOnline = test > 0 ? true : false;
            //    }
            //}
            //catch
            //{
            //    Globals.WorkingOnline = false;
            //}

            ClockTimer.Interval = 5000;	// 5 seconds
            ClockTimer.Enabled = true;
            statusBarClock.Text = DateTime.Now.ToShortTimeString();
            bInitializing = false;
        }
        private void OnResizeMainForm(object sender, EventArgs e)
        {
            CenterControls();
            if (bInitializing)
                return;

            listViewSource.Columns["FullPath"].Width = listViewSource.ClientRectangle.Width;
            listViewOutputDir.Columns["FullPath"].Width = listViewOutputDir.ClientRectangle.Width - 60;
            listViewCompanyNames.Columns["CompanyNames"].Width = listViewCompanyNames.ClientRectangle.Width;
        }

        #endregion Form Events

        #region Click Events
        private void OnClickExit(object sender, EventArgs e)
        {
            CloseApplication();
        }
        private void OnClickAbout(object sender, EventArgs e)
        {
            using (AboutBox dlg = new AboutBox())
            {
                dlg.ShowDialog();
            }
        }
        private void OnClosing(object sender, FormClosingEventArgs e)
        {
            SaveConfiguration();
        }
        private void OnClickBuildBaseDirs(object sender, EventArgs e)
        {
            statusBarStatus.Text = "Gathering Directory and Company Information";

            // clear out all the data list so there isn't anything hanging out there from prior run
            listViewCompanyNames.Items.Clear();
            listViewOutputDir.Items.Clear();
            listViewSource.Items.Clear();
            directories.Clear();
            Globals.CompanyDetails.Clear();
            Globals.SiteDetails.Clear();
            Globals.UnitDetails.Clear();

            if (Globals.VALID_YEAR_LOWERLIMIT > Int32.Parse(txtOutputYear.Text))
            {
                btnCreateCompanyDirs.Enabled = false;
                btnCreateSiteDirs.Enabled = false;
                MessageBox.Show(String.Format("Year must be greater than {0}", Globals.VALID_YEAR_LOWERLIMIT));
                return;
            }
            else if (Int32.Parse(txtOutputYear.Text) > DateTime.Now.Year)
            {
                btnCreateCompanyDirs.Enabled = false;
                btnCreateSiteDirs.Enabled = false;
                MessageBox.Show("Year can not be greater than current year");
                return;
            }

            Globals.CurrentYear = txtOutputYear.Text;

            using (BackgroundWorker bgwkrCompanyList = new BackgroundWorker { WorkerReportsProgress = false, WorkerSupportsCancellation = false })
            {
                bgwkrCompanyList.DoWork += bgwkrCompanyList_DoWork;
                bgwkrCompanyList.RunWorkerCompleted += OnRunWorkerCompletedbgwkrCompanyList;
                bgwkrCompanyList.RunWorkerAsync();
            }

            using (BackgroundWorker bgwkrBasicDir = new BackgroundWorker { WorkerReportsProgress = false, WorkerSupportsCancellation = false })
            {
                bgwkrBasicDir.DoWork += bgwkrBasicDir_DoWork;
                bgwkrBasicDir.RunWorkerCompleted += OnRunWorkerCompletedbgwkrBasicDir;
                bgwkrBasicDir.RunWorkerAsync();
            }

        }
        private void OnClickCreateCompanyDirs(object sender, EventArgs e)
        {
            statusBarStatus.Text = "Building Company Structure";
            Cursor = Cursors.WaitCursor;

            CreateCompanyStructure();

            statusBarStatus.Text = "Ready";
            Cursor = Cursors.Default;
        }
        private void OnClickCreateSiteDirs(object sender, EventArgs e)
        {
            statusBarStatus.Text = "Building Site Structure";
            Cursor = Cursors.WaitCursor;

            CreateSiteStructure();
            CreateShortcuts();

            statusBarStatus.Text = "Ready";
            Cursor = Cursors.Default;
        }
        private void OnDoubleClickCompanyNames(object sender, EventArgs e)
        {
            ListView.SelectedListViewItemCollection currentCompany = listViewCompanyNames.SelectedItems;
            using (CompanyDetailForm dlg = new CompanyDetailForm())
            {
                foreach (ListViewItem item in currentCompany)
                    dlg.CompanyDetailName = item.Text;
                dlg.ShowDialog();
            }
        }

        #endregion Click Events

        #region Methods
        private void LoadConfiguration()
        {
            // Apply Configuration Data
            Globals.bRunningLocal = ((ConfigurationManager.AppSettings["Local"].ToUpper() == "TRUE") ? true : false);
            Globals.DataSource = Globals.bRunningLocal ? "(local)" : ConfigurationManager.AppSettings["DataSource"];
            Globals.InitialDirectory = ConfigurationManager.AppSettings["InitialDirectory"];
            Globals.TempDirectory = ConfigurationManager.AppSettings["TempDirectory"];
            Globals.BasePath = ConfigurationManager.AppSettings["BasePath"];
            Globals.connString = ConfigurationManager.AppSettings["ConnString"];

            // read in the application userid and password
            string _uid = string.Empty;
            string _pwd = string.Empty;
            _uid = ConfigurationManager.AppSettings["UserID"];
            _pwd = ConfigurationManager.AppSettings["Password"];

            Globals.UserID = ComUtilsAlias.Security.Encryption.Decrypt(_uid, Globals.EncryptionKey, Globals.EncryptionIV);
            Globals.Password = ComUtilsAlias.Security.Encryption.Decrypt(_pwd, Globals.EncryptionKey, Globals.EncryptionIV);

            // read in my userid and password
            _uid = string.Empty;
            _pwd = string.Empty;
            _uid = ConfigurationManager.AppSettings["UserIDDB"];
            _pwd = ConfigurationManager.AppSettings["PasswordDB"];

            Globals.UserIDDB = ComUtilsAlias.Security.Encryption.Decrypt(_uid, Globals.EncryptionKey, Globals.EncryptionIV);
            Globals.PasswordDB = ComUtilsAlias.Security.Encryption.Decrypt(_pwd, Globals.EncryptionKey, Globals.EncryptionIV);

            string _formset = string.Empty;
            _formset = ConfigurationManager.AppSettings["Formset"];
            if (_formset.Length > 0)
            {
                string[] _settings = _formset.Split(',');
                if (_settings.Count() == 4)
                {
                    Location = new Point(int.Parse(_settings[0]), int.Parse(_settings[1]));
                    Size = new Size(int.Parse(_settings[2]), int.Parse(_settings[3]));
                }
            }

            // now get the list of valid Developers Users
            string _Developers = ConfigurationManager.AppSettings["Developers"];
            string[] lstDevelopers = ComUtilsAlias.Security.Encryption.Decrypt(_Developers, Globals.EncryptionKey, Globals.EncryptionIV).Split(';');

            Array.ForEach(lstDevelopers, str => Globals.Developers.Add(str.ToUpper()));

            // now get the list of valid Administrator Users
            string _Administrators = ConfigurationManager.AppSettings["Administrators"];
            string[] lstAdministrators = ComUtilsAlias.Security.Encryption.Decrypt(_Administrators, Globals.EncryptionKey, Globals.EncryptionIV).Split(';');

            Array.ForEach(lstAdministrators, str => Globals.Administrators.Add(str.ToUpper()));

            // now get the list of valid PowerUser Users
            string _PowerUsers = ConfigurationManager.AppSettings["PowerUsers"];
            string[] lstPowerUsers = ComUtilsAlias.Security.Encryption.Decrypt(_PowerUsers, Globals.EncryptionKey, Globals.EncryptionIV).Split(';');

            Array.ForEach(lstPowerUsers, str => Globals.PowerUsers.Add(str.ToUpper()));
        }
        private void SaveConfiguration()
        {
            Globals.StatusMessageText = "Saving Form configuration settings";
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            StringBuilder formInfo = new StringBuilder();
            Rectangle screen = That.Computer.Screen.Bounds;
            int x = Location.X;
            int y = Location.Y;
            if ((x < screen.Left) || (x > screen.Right))
                x = 0;
            if ((y < screen.Top) || (y > screen.Bottom))
                y = 0;
            formInfo.Append(String.Format("{0},", x));
            formInfo.Append(String.Format("{0},", y));
            formInfo.Append(String.Format("{0},", Size.Width));
            formInfo.Append(String.Format("{0}", Size.Height));

            config.AppSettings.Settings.Remove("Formset");
            config.AppSettings.Settings.Add("Formset", formInfo.ToString());

            // Save the configuration file.
            config.Save(ConfigurationSaveMode.Modified);

            //// Force a reload of a changed section.
            ConfigurationManager.RefreshSection("appSettings");

            Globals.StatusMessageText = "";
        }
        /// <summary>
        /// Updates the StatusBarStatus text and forces a form update()
        /// </summary>
        /// <param name="_msg">text to display to user</param>
        private void StatusMessageTextChanged(object sender, EventArgs e)
        {
            statusBarStatus.Text = Globals.StatusMessageText;
            Update();
        }
        private static void CloseApplication()
        {
            Application.Exit();
        }

        private void OnRunWorkerCompletedbgwkrCompanyList(object sender, RunWorkerCompletedEventArgs e)
        {
            Globals.CompanyDetails.ForEach(ci => listViewCompanyNames.Items.Add(ci.Name));
            btnCreateCompanyDirs.Enabled = true;
            btnCreateSiteDirs.Enabled = true;
            statusBarStatus.Text = "Ready";
        }
        private void bgwkrCompanyList_DoWork(object sender, DoWorkEventArgs e)
        {
            CreateCurrentYrCompanyList();
        }
        private void OnRunWorkerCompletedbgwkrBasicDir(object sender, RunWorkerCompletedEventArgs e)
        {
            directories.ForEach(dir => listViewSource.Items.Add(dir));

            if (e.Error != null)
            {
                // There was an error during the operation.
                string msg = String.Format("An error occurred: {0}", e.Error.Message);
                MessageBox.Show(msg);
            }
            else
            {
                List<ListViewItem> ItemsList = new List<ListViewItem>();
                ItemsList = (List<ListViewItem>)e.Result;
                ItemsList.ForEach(lvi => listViewOutputDir.Items.Add(lvi));
            }
        }
        private void bgwkrBasicDir_DoWork(object sender, DoWorkEventArgs e)
        {
            e.Result = CreateBasicDirectories();
        }
        private void CenterControls()
        {
            ComUIUtilsAlias.Utils.CenterControlItems(pnlTop);
            ComUIUtilsAlias.Utils.CenterControlItems(pnlBottom);
        }
        //private void PopulateTreeView(string path)
        //{
        //    statusBarStatus.Text = "Building Source Directory Structure";
        //    Cursor = Cursors.WaitCursor;
        //    TreeNode rootNode;

        //    DirectoryInfo info = new DirectoryInfo(path);
        //    if (info.Exists)
        //    {
        //        rootNode = new TreeNode(info.Name);
        //        rootNode.Tag = info;
        //        GetDirectories(info.GetDirectories(), rootNode);
        //        treeViewSource.Nodes.Add(rootNode);
        //    }
        //    statusBarStatus.Text = "Completed Building Source Directory Structure";
        //    Cursor = Cursors.Default;
        //}
        private void GetDirectories(DirectoryInfo[] subDirs,
            TreeNode nodeToAddTo)
        {
            TreeNode aNode;
            DirectoryInfo[] subSubDirs;
            foreach (DirectoryInfo subDir in subDirs)
            {
                aNode = new TreeNode(subDir.Name, 0, 0);
                directories.Add(subDir.FullName);

                aNode.Tag = subDir;
                aNode.ImageKey = "folder";
                switch (subDir.Name)
                {
                    case "Company":
                    case "Company Correspondence":
                    case "Correspondence":
                    case "Pending":
                    case "Plant":
                    case "Plant Names":
                    case "PowerCDs":
                    case "Presentations":
                        break;
                    default:
                        subSubDirs = subDir.GetDirectories();
                        if (subSubDirs.Length != 0)
                        {
                            GetDirectories(subSubDirs, aNode);
                        }
                        break;
                }
                nodeToAddTo.Nodes.Add(aNode);
            }
        }

        private void CreateCurrentYrCompanyList()
        {
            ignoreFiles.Clear();
            PowerCDDir = String.Format(@"{0}{1}\PowerCDs", Globals.BasePath, Globals.CurrentYear);
            DirectoryInfo DirPowerCDs = new DirectoryInfo(PowerCDDir);
            DirectoryInfo[] subCompanyDirs = DirPowerCDs.GetDirectories();

            //// create list of files to ignore
            //Array.ForEach(subCompanyDirs,
            //delegate(DirectoryInfo subDir)
            //{
            //    if (("CC" == subDir.Name.ToUpper()) || ("STEAM" == subDir.Name.ToUpper()))
            //    {
            //        if ("CC" == subDir.Name.ToUpper())
            //        {
            //            PwrCCMasterFile = PwrCCMasterFile.Replace("XX", Globals.CurrentYear2Char);
            //            PwrCCMasterFile = String.Format(@"{0}\{1}", subDir.FullName, PwrCCMasterFile);
            //        }
            //        else
            //        {
            //            PwrStmMasterFile = PwrStmMasterFile.Replace("XX", Globals.CurrentYear2Char);
            //            PwrStmMasterFile = String.Format(@"{0}\{1}", subDir.FullName, PwrStmMasterFile);
            //        }
            //        Array.ForEach(subDir.GetFiles(),
            //        delegate(FileInfo fi)
            //        {
            //            if (!ignoreFiles.Contains(fi.Name))
            //                ignoreFiles.Add(fi.Name);
            //        });
            //    }
            //});

            Array.ForEach(subCompanyDirs,
            delegate(DirectoryInfo subDir)
            {
                if (("CC" == subDir.Name.ToUpper()) || ("STEAM" == subDir.Name.ToUpper()) || ("INPUT FORMS" == subDir.Name.ToUpper()))
                {
                    // skip these master directories but create list of files to ignore
                }
                else
                {
                    //listViewCompanyNames.Items.Add(subDir.Name);
                    SiteInfo siteinfo = new SiteInfo();
                    string newYR = Globals.CurrentYear2Char;
                    string _name;
                    string _parent;
                    string _oldSiteId;
                    string _newSiteId;
                    //DirectoryInfo[] subsubDirs = subDir.GetDirectories();
                    //Array.ForEach(subsubDirs,
                    //delegate(DirectoryInfo di)
                    //{
                    //    Array.ForEach(di.GetFiles(),
                    //    delegate(FileInfo fi)
                        Array.ForEach(subDir.GetFiles(),
                        delegate(FileInfo fi)
                        {
                            System.Diagnostics.Debug.WriteLine(String.Format("{0} -- {1}", fi.Name, subDir.Name));
                            if (!ignoreFiles.Contains(fi.Name))
                                if (!fi.Name.StartsWith("PwrMacro"))
                                {
                                    int found = fi.Name.IndexOf(".xls");
                                    if (found > 1)
                                    {
                                        _name = fi.Name.Substring(0, found);
                                        _parent = subDir.Name;
                                        System.Diagnostics.Debug.WriteLine(String.Format("{0} -- {1}", _name, _parent));
                                        _oldSiteId = GetSiteID(_parent, _name);
                                        if (0 == _oldSiteId.Length)
                                            MessageBox.Show(String.Format("Site Name [{0}] not found.", _name));
                                        else
                                        {
                                            // now build the new SiteID value
                                            if (_oldSiteId.EndsWith("P"))
                                                _newSiteId = _oldSiteId.Remove(_oldSiteId.Length - 3, 3);
                                            else
                                                _newSiteId = _oldSiteId.Remove(_oldSiteId.Length - 2, 2);
                                            _newSiteId += newYR;
                                            Globals.SiteDetails.Add(new SiteInfo { Name = _name, SiteID = _newSiteId, PriorSiteID = _oldSiteId, Parent = _parent });
                                        }
                                    }
                                }
                        });
                    //});
                    Globals.CompanyDetails.Add(new CompanyInfo { Name = subDir.Name });
                }
            });
            Common.Linq.ObjectDumper.Write(Globals.SiteDetails);
            //foreach (CompanyInfo ci in Globals.CompanyDetails)
            //{
            //    listViewCompanyNames.Items.Add(ci.Name);
            //}
            //statusBarStatus.Text = "Ready";
        }
        private List<ListViewItem> CreateBasicDirectories()
        {
            List<ListViewItem> lstItems = new List<ListViewItem>();
            ListViewItem lvi;

            Globals.BaseDirectoryList.ForEach(dir => directories.Add(Globals.BasePath + txtOutputYear.Text + dir));

            directories.ForEach(delegate(string name)
            {
                lvi = new ListViewItem(name);
                CreateDirAndAddSubItemToListView(name, lvi);
                lstItems.Add(lvi);
            });
            return lstItems;
        }
        private void CreateCompanyStructure()
        {
            string newPath;
            ListViewItem lvi;

            // add company names to:
            //"Company"
            //"Company Correspondence"
            //"Presentations"
            Globals.CompanyDetails.ForEach(delegate(CompanyInfo ci)
            {
                newPath = String.Format(@"{0}{1}\Company\{2}", Globals.BasePath, txtOutputYear.Text, ci.Name);
                lvi = new ListViewItem(newPath);
                CreateDirAndAddSubItemToListView(newPath, lvi);
                if (!listViewOutputDir.Items.ContainsKey(newPath))
                    listViewOutputDir.Items.Add(lvi);
                newPath = String.Format(@"{0}{1}\Company Correspondence\{2}", Globals.BasePath, txtOutputYear.Text, ci.Name);
                lvi = new ListViewItem(newPath);
                CreateDirAndAddSubItemToListView(newPath, lvi);
                if (!listViewOutputDir.Items.ContainsKey(newPath))
                    listViewOutputDir.Items.Add(lvi);
                newPath = String.Format(@"{0}{1}\Presentations\{2}", Globals.BasePath, txtOutputYear.Text, ci.Name);
                lvi = new ListViewItem(newPath);
                CreateDirAndAddSubItemToListView(newPath, lvi);
                if (!listViewOutputDir.Items.ContainsKey(newPath))
                    listViewOutputDir.Items.Add(lvi);
            });
        }
        private static void CreateDirAndAddSubItemToListView(string path, ListViewItem lvi)
        {
            DirectoryInfo subDir = new DirectoryInfo(path);
            if (subDir.Exists)
            {
                lvi.SubItems.Add("Exist");
            }
            else
            {
                string status = string.Empty;
                try
                {
                    subDir.Create();
                    status = "Created";
                }
                catch (Exception ex)
                {
                    status = String.Format("Error{0}", ex.Message);
                }
                finally
                {
                    lvi.SubItems.Add(status);
                }
            }
        }
        private void CreateSiteStructure()
        {
            string newPath;
            ListViewItem lvi;

            // add Sites to list
            // "Correspondence"
            // "Plant"
            Globals.SiteDetails.ForEach(delegate(SiteInfo si)
            {
                if ("" == si.SiteID)
                    MessageBox.Show(String.Format("No SiteID for this Site: {0}: {1}", si.Parent, si.Name));
                else
                {
                    newPath = String.Format(@"{0}{1}\Correspondence\{2}", Globals.BasePath, txtOutputYear.Text, si.SiteID);
                    lvi = new ListViewItem(newPath);
                    CreateDirAndAddSubItemToListView(newPath, lvi);
                    if (!listViewOutputDir.Items.ContainsKey(newPath))
                        listViewOutputDir.Items.Add(lvi);
                    newPath = String.Format(@"{0}{1}\Plant\{2}", Globals.BasePath, txtOutputYear.Text, si.SiteID);
                    lvi = new ListViewItem(newPath);
                    CreateDirAndAddSubItemToListView(newPath, lvi);
                    // check to see if the ListView already contains the directory string
                    if (!listViewOutputDir.Items.ContainsKey(newPath))
                        listViewOutputDir.Items.Add(lvi);
                    // check to see if the Powerxx or PowerCCxx files exist in the SiteID directory
                    // if not, copy on from PowerCD subdirectory in there for other processes to use later
                    string srcFilepath = string.Empty;
                    string xlsTemplate = string.Empty;
                    if (si.SiteID.IndexOf("PN") > 2)
                        srcFilepath = PwrStmMasterFile.Replace("XX", Globals.CurrentYear2Char);
                    else
                        srcFilepath = PwrCCMasterFile.Replace("XX", Globals.CurrentYear2Char);
                    xlsTemplate = srcFilepath.Substring(srcFilepath.LastIndexOf(@"\") + 1);
                    if (!File.Exists(String.Format(@"{0}\{1}", newPath, xlsTemplate)))
                        try
                        {
                            File.Copy(srcFilepath, String.Format(@"{0}\{1}", newPath, xlsTemplate));
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(String.Format("Error while copying xls template file: {0}", ex.Message));
                        }
                }
            });
        }
        private static string GetSiteID(string parent, string name)
        {
            string siteID = string.Empty;
            string tmpID;
            string tmpYr;

            string tmpSQL = Globals.strSQLGetSiteIDbyCompanyIDSiteName.Replace("xxxCompanyID", parent);
            tmpSQL = tmpSQL.Replace("xxxSiteName", name);
            try
            {
                Globals.dsStudySites = DBUtilsAlias.DatabaseAccess.GetDataset(Globals.sqlConnPW, tmpSQL, "SiteID");

                if (Globals.dsStudySites.Tables[0].Rows.Count > 0)
                {
                    tmpID = DBUtilsAlias.Datasets.GetString(Globals.dsStudySites.Tables[0].Rows[0]["SiteID"]);
                    siteID = tmpID.Trim().ToUpper();

                    // if it has a "P" then figure out if it is more current than one without a "P"
                    if (siteID.EndsWith("P"))
                    {
                        tmpID = DBUtilsAlias.Datasets.GetString(Globals.dsStudySites.Tables[0].Rows[1]["SiteID"]);
                        tmpID = tmpID.Trim();
                        // get year portion based on if it ends in "P" or not
                        if (tmpID.EndsWith("P"))
                        {
                            tmpYr = tmpID.Substring(tmpID.Length - 3, 2);
                        }
                        else
                        {
                            tmpYr = tmpID.Substring(tmpID.Length - 2, 2);
                        }
                        if (tmpYr == siteID.Substring(siteID.Length - 3, 2))
                        {
                            siteID = tmpID;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Problem getting data from SQL: {0}", ex.Message));
                return siteID;
            }
            return siteID;
        }
        private static void CreateShortcuts()
        {
            string lnkBaseDir = Globals.BasePath + Globals.CurrentYear;
            string trgBasePath = Globals.BasePath + Globals.CurrentYear;
            string lnkDir = string.Empty;
            string lnkFileName = string.Empty;
            string trgPath = string.Empty;
            string siteName = string.Empty;
            string siteID = string.Empty;
            string companyName = string.Empty;

            foreach (SiteInfo si in Globals.SiteDetails)
            {
                companyName = si.Parent;
                siteName = si.Name;
                siteID = si.SiteID;

                // check to make sure data is valid
                if ((0 == companyName.Length) || (0 == siteID.Length) || (0 == siteName.Length))
                {
                    StringBuilder sbError = new StringBuilder();
                    sbError.Append("Problem with data:\n");
                    sbError.Append("Company: ");
                    sbError.Append(String.Format("{0}\n", companyName));
                    sbError.Append("SiteName: ");
                    sbError.Append(String.Format("{0}\n", siteName));
                    sbError.Append("SiteID: ");
                    sbError.Append(String.Format("{0}\n", siteID));
                    MessageBox.Show(sbError.ToString());
                    break;
                }

                #region create "Correspondence" shortcuts
                // filename=\\Dallas1\data\Data\STUDY\Power\2007\Correspondence\933CC07\__NUON     Hemweg 07 (933CC07)
                // targetpath=\\Dallas1\data\Data\STUDY\Power\2007\Correspondence\933CC07
                lnkDir = String.Format(@"{0}\Correspondence\{1}", lnkBaseDir, siteID);
                lnkFileName = String.Format(@"__{0}  {1} ({2})", companyName, siteName, siteID);
                trgPath = String.Format(@"{0}\Correspondence\{1}", trgBasePath, siteID);
                if (!Common.Utility.ShellLinks.Link.Exists(lnkDir, lnkFileName))
                {
                    try
                    {
                        Common.Utility.ShellLinks.Link.Update(lnkDir, trgPath, lnkFileName, true);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }

                // filename=\\Dallas1\DATA\Data\STUDY\Power\2007\Plant Names\_Data
                // targetpath=\\Dallas1\data\Data\STUDY\Power\2007\Plant\933CC07
                lnkFileName = "_Data";
                trgPath = String.Format(@"{0}\Plant\{1}", trgBasePath, siteID);
                if (!Common.Utility.ShellLinks.Link.Exists(lnkDir, lnkFileName))
                {
                    try
                    {
                        Common.Utility.ShellLinks.Link.Update(lnkDir, trgPath, lnkFileName, true);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }

                #endregion create "Correspondence" shortcuts

                #region create "Plant Names" shortcuts
                // filename=\\Dallas1\DATA\Data\STUDY\Power\2007\Plant Names\NUON     Hemweg 07 (933CC07)
                // targetpath=\\Dallas1\data\Data\STUDY\Power\2007\Plant\933CC07
                lnkDir = String.Format(@"{0}\Plant Names\", lnkBaseDir);
                lnkFileName = String.Format("{0}  {1} ({2})", companyName, siteName, siteID);
                trgPath = String.Format(@"{0}\Plant\{1}", trgBasePath, siteID);
                if (!Common.Utility.ShellLinks.Link.Exists(lnkDir, lnkFileName))
                {
                    try
                    {
                        Common.Utility.ShellLinks.Link.Update(lnkDir, trgPath, lnkFileName, true);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }

                #endregion create "Plant Names" shortcuts

                #region create "Plant" shortcuts
                // filename=\\Dallas1\data\Data\STUDY\Power\2007\Plant\933CC07\_Correspondence
                // targetpath=\\Dallas1\data\Data\STUDY\Power\2007\Correspondence\933CC07
                lnkDir = String.Format(@"{0}\Plant\{1}", lnkBaseDir, siteID);
                lnkFileName = @"_Correspondence";
                trgPath = String.Format(@"{0}\Correspondence\{1}", trgBasePath, siteID);
                if (!Common.Utility.ShellLinks.Link.Exists(lnkDir, lnkFileName))
                {
                    try
                    {
                        Common.Utility.ShellLinks.Link.Update(lnkDir, trgPath, lnkFileName, true);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }

                // filename=\\Dallas1\data\Data\STUDY\Power\2007\Plant\933CC07\__NUON     Hemweg 07 (933CC07)
                // targetpath=\\Dallas1\data\Data\STUDY\Power\2007\Plant\933CC07
                lnkDir = String.Format(@"{0}\Plant\{1}", lnkBaseDir, siteID);
                lnkFileName = String.Format(@"__{0}  {1} ({2})", companyName, siteName, siteID);
                trgPath = String.Format(@"{0}\Plant\{1}", trgBasePath, siteID);
                if (!Common.Utility.ShellLinks.Link.Exists(lnkDir, lnkFileName))
                {
                    try
                    {
                        Common.Utility.ShellLinks.Link.Update(lnkDir, trgPath, lnkFileName, true);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }

                #endregion create "Plant" shortcuts
            }
        }

        #endregion Methods

        private void button1_Click(object sender, EventArgs e)
        {

            // now get the list of valid Developers Users
            string Devs = ConfigurationManager.AppSettings["Developers"];
            string[] lstDevs = ComUtilsAlias.Security.Encryption.Decrypt(Devs, Globals.EncryptionKey, Globals.EncryptionIV).Split(';');

            Array.ForEach(lstDevs, str => Globals.Developers.Add(str.ToUpper()));

            // now get the list of valid Administrator Users
            string Adms = ConfigurationManager.AppSettings["Administrators"];
            string[] lstAdms = ComUtilsAlias.Security.Encryption.Decrypt(Adms, Globals.EncryptionKey, Globals.EncryptionIV).Split(';');

            Array.ForEach(lstAdms, str => Globals.Administrators.Add(str.ToUpper()));

            Array.ForEach(lstDevs, str => listViewOutputDir.Items.Add(str));
            Array.ForEach(lstAdms, str => listViewOutputDir.Items.Add(str));
        }

        private void listViewCompanyNames_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        #region Timer


        #endregion Timer

    }
}
