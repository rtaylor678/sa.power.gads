﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace PowerCPA
{
    public partial class CompanyDetailForm : Form
    {
        #region Variables
        #region Local Variables
        #endregion Local Variables

        #region Global Variables

        #endregion Global Variables

        #endregion Variables

        #region Public Properties
        public string CompanyDetailName { private get; set; }

        #endregion Public Properties

        #region Constructors
        public CompanyDetailForm()
        {
            InitializeComponent();
            treeViewCompanyDetail.ExpandAll();
        }
        #endregion Constructors

        #region Form Events
        private void OnLoad(object sender, EventArgs e)
        {
            btnCreateRecords.Enabled = false;
            statusBarMessage.Text = "Collecting company information";

            using (BackgroundWorker bgwkrCompanyInfo = new BackgroundWorker { WorkerReportsProgress = false, WorkerSupportsCancellation = false })
            {
                bgwkrCompanyInfo.DoWork += bgwkrCompanyInfo_DoWork;
                bgwkrCompanyInfo.RunWorkerCompleted += OnRunWorkerCompletedbgwkrCompanyInfo;
                bgwkrCompanyInfo.RunWorkerAsync();
            }
        }

        private void OnClickClose(object sender, EventArgs e)
        {
            Close();
        }

        private void OnClickCreateRecords(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            CreateDatabaseRecords();
            Cursor = Cursors.Default;
            Close();
        }

        #endregion Form Events3

        #region Methods
        private void OnRunWorkerCompletedbgwkrCompanyInfo(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                // There was an error during the operation.
                string msg = String.Format("An error occurred: {0}", e.Error.Message);
                MessageBox.Show(msg);
            }
            else
            {
                treeViewCompanyDetail.Nodes.Add((TreeNode)e.Result);
                treeViewCompanyDetail.ExpandAll();
                btnCreateRecords.Enabled = true;
                statusBarMessage.Text = "Ready";
            }
        }
        private void bgwkrCompanyInfo_DoWork(object sender, DoWorkEventArgs e)
        {
            e.Result = BuildTree();
        }
        private TreeNode BuildTree()
        {
            TreeNode rootNode = new TreeNode(CompanyDetailName) { };
            var querySites = from SiteInfo in Globals.SiteDetails
                             where (SiteInfo.Parent == CompanyDetailName)
                             orderby SiteInfo.SiteID
                             select new
                             {
                                 name = SiteInfo.Name.Trim(),
                                 siteid = SiteInfo.SiteID.Trim(),
                                 parent = SiteInfo.Parent.Trim(),
                                 priorsiteid = SiteInfo.PriorSiteID.Trim()
                             };

            foreach (var item in querySites)
            {
                TreeNode siteNode = new TreeNode(String.Format("{0} ({1})", item.name, item.siteid));
                using (var dc = new PowerWorkDataContext(Globals.sqlConnPW.ConnectionString))//, IsolationLevel.Snapshot))
                {
                    var queryUnits = Queries.GetTSortBySiteID(dc, item.priorsiteid);
                    foreach (TSort tsRow in queryUnits)
                    {
                        TreeNode unitNode = new TreeNode(String.Format("{0} ({1})", tsRow.CoLoc, tsRow.Refnum.Trim()));
                        var queryComponents = Queries.GetNERCTurbineByRefnum(dc, tsRow.Refnum);
                        foreach (NERCTurbine ntRow in queryComponents)
                        {
                            unitNode.Nodes.Add(new TreeNode(String.Format("{0} ({1})", ntRow.TurbineID.Trim(), ntRow.UtilityUnitCode.Trim())));
                        }
                        siteNode.Nodes.Add(unitNode);
                    }
                }
                rootNode.Nodes.Add(siteNode);
            }
            return rootNode;
        }
        private void CreateDatabaseRecords()
        {
            int yearPortion = 2;
            int currYr = Int32.Parse(Globals.CurrentYear);

            var querySiteDetails = from SiteInfo in Globals.SiteDetails
                                   where (SiteInfo.Parent == CompanyDetailName)
                                   orderby SiteInfo.SiteID
                                   select SiteInfo;

            foreach (var site in querySiteDetails)
            {
                string siteid = site.SiteID;
                string oldSiteid = site.PriorSiteID;
                // now create new refnum
                if (oldSiteid.EndsWith("P"))
                {
                    yearPortion = 3;
                }

                using (PowerWorkDataContext dcPW = new PowerWorkDataContext(Globals.sqlConnPW.ConnectionString))
                {
                    dcPW.Log = Console.Out;
                    // create the StudySite record
                    BuildStudySiteRecord(yearPortion, currYr, siteid, oldSiteid, dcPW);
                    // create the ClientInfo record
                    BuildClientInfoRecord(siteid, oldSiteid, dcPW);
                    // create the Process Checklist records
                    BuildCheckListRecords(siteid, dcPW);
                    // create the TSort records
                    BuildTSortRecords(yearPortion, currYr, siteid, oldSiteid, dcPW);
                    try
                    {
                        dcPW.SubmitChanges(System.Data.Linq.ConflictMode.FailOnFirstConflict);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(String.Format("Error while saving data to database. {0}", ex.Message));
                    }
                }
            }
        }

        /// <summary>
        /// -copy latest record (no P if possible) for each SITE
        /// -change the following fields
        /// 	SiteID		- change the last 2 digits to current yr
        /// 	StudyYear	- change to current yr
        /// 	SiteDirectory	- change to "Power\2006" to "Power\current yr" and SiteID at the end of the path
        /// </summary>
        /// <param name="yearPortion">either a 2 or 3 based on whether the old siteid has a "P" or not</param>
        /// <param name="currYr">Int32.Parse(Globals.CurrentYear)</param>
        /// <param name="siteid">new siteid</param>
        /// <param name="oldSiteid">old siteid</param>
        /// <param name="dcPW">PowerWork DataContext</param>
        private static void BuildStudySiteRecord(int yearPortion, int currYr, string siteid, string oldSiteid, PowerWorkDataContext dcPW)
        {
            IQueryable queryStudySites = Queries.GetStudySitesBySiteID(dcPW, oldSiteid);
            foreach (StudySite item in queryStudySites)
            {
                bool siteidExist = false;
                StudySite ssRow = new StudySite();
                yearPortion = 2;

                // check to see if the SiteID record already exist for new SiteID, if so skip
                IQueryable querySiteID = Queries.GetStudySitesBySiteID(dcPW, siteid);
                foreach (StudySite itemSiteID in querySiteID)
                {
                    siteidExist = true;
                    break;
                }
                if (siteidExist)
                    break;

                // fields that need to change from prior year
                ssRow.SiteID = siteid;
                ssRow.StudyYear = currYr;
                string newSiteDir = item.SiteDirectory;
                newSiteDir = newSiteDir.Remove(newSiteDir.IndexOf(@"\Power\") + 7);
                newSiteDir += String.Format(@"{0}\Plant\{1}", currYr, siteid);
                ssRow.SiteDirectory = newSiteDir;

                // fields that do not need to change from prior year
                ssRow.CompanyID = item.CompanyID;
                ssRow.Consultant = item.Consultant;
                ssRow.SiteLabel = item.SiteLabel;
                ssRow.SiteName = item.SiteName;
                ssRow.SiteNo = item.SiteNo;

                dcPW.StudySites.InsertOnSubmit(ssRow);
                //dcPW.SubmitChanges();
            }
        }
        /// <summary>
        /// -copy latest record (no P if possible) for each SITE
        /// -change the following fields
        /// 	SiteID		- change the last 2 digits to current yr
        /// </summary>
        /// <param name="siteid">new siteid</param>
        /// <param name="oldSiteid">old siteid</param>
        /// <param name="dcPW">PowerWork DataContext</param>
        private static void BuildClientInfoRecord(string siteid, string oldSiteid, PowerWorkDataContext dcPW)
        {
            IQueryable queryStudySites = Queries.GetClientInfoBySiteID(dcPW, oldSiteid);
            foreach (ClientInfo item in queryStudySites)
            {
                bool siteidExist = false;
                ClientInfo ciRow = new ClientInfo();
                //yearPortion = 2;

                // check to see if the SiteID record already exist for new SiteID, if so skip
                IQueryable querySiteID = Queries.GetClientInfoBySiteID(dcPW, siteid);
                foreach (ClientInfo itemSiteID in querySiteID)
                {
                    siteidExist = true;
                    break;
                }
                if (siteidExist)
                    break;

                // fields that need to change from prior year
                ciRow.SiteID = siteid;

                // fields that do not need to change from prior year
                ciRow.AffName = item.AffName;
                ciRow.City = item.City;
                ciRow.CoordAddr1 = item.CoordAddr1;
                ciRow.CoordAddr2 = item.CoordAddr2;
                ciRow.CoordCity = item.CoordCity;
                ciRow.CoordEMail = item.CoordEMail;
                ciRow.CoordFax = item.CoordFax;
                ciRow.CoordName = item.CoordName;
                ciRow.CoordPhone = item.CoordPhone;
                ciRow.CoordState = item.CoordState;
                ciRow.CoordTitle = item.CoordTitle;
                ciRow.CoordZip = item.CoordZip;
                ciRow.CorpName = item.CorpName;
                ciRow.PlantName = item.PlantName;
                ciRow.State = item.State;

                dcPW.ClientInfos.InsertOnSubmit(ciRow);
                //dcPW.SubmitChanges();
            }
        }
        /// <summary>
        /// -copy latest record (no P if possible) for EACH refnum in study
        /// -change the following fields
        /// 	Refnum		- change the last 2 digits to current yr
        /// 	SiteID		- change the last 2 digits to current yr
        /// 	StudyYear	- change to current yr
        /// 	EvntYear	- change to current yr
        /// </summary>
        /// <param name="yearPortion">either a 2 or 3 based on whether the old siteid has a "P" or not</param>
        /// <param name="currYr">Int32.Parse(Globals.CurrentYear)</param>
        /// <param name="siteid">new siteid</param>
        /// <param name="oldSiteid">old siteid</param>
        /// <param name="dcPW">PowerWork DataContext</param>
        private static void BuildTSortRecords(int yearPortion, int currYr, string siteid, string oldSiteid, PowerWorkDataContext dcPW)
        {
            IQueryable query = Queries.GetTSortBySiteID(dcPW, oldSiteid);
            foreach (TSort item in query)
            {
                bool refnumExist = false;
                TSort tsRow = new TSort();
                yearPortion = 2;

                string newRefnum = item.Refnum.Trim();
                // now create new refnum
                if (newRefnum.EndsWith("P"))
                {
                    yearPortion = 3;
                }

                newRefnum = newRefnum.Remove(newRefnum.Length - yearPortion);
                newRefnum += Globals.CurrentYear.Substring(2);

                // check to see if the refnum record already exist for new SiteID, if so skip
                IQueryable queryRefnum = Queries.GetTSortByRefnum(dcPW, newRefnum);
                foreach (TSort itemRefnum in queryRefnum)
                {
                    refnumExist = true;
                    break;
                }
                if (refnumExist)
                    break;

                // fields that need to change from prior year
                tsRow.Refnum = newRefnum;
                tsRow.SiteID = siteid;
                tsRow.StudyYear = currYr;
                tsRow.EvntYear = currYr;
                tsRow.CalcCommUnavail = 'Y';

                // fields that stay the same
                tsRow.Coal_NDC = item.Coal_NDC;
                tsRow.CoLoc = item.CoLoc;
                tsRow.CompanyID = item.CompanyID;
                tsRow.CompanyName = item.CompanyName;
                tsRow.Continent = item.Continent;
                tsRow.Country = item.Country;
                tsRow.CTG_NDC = item.CTG_NDC;
                tsRow.CTGs = item.CTGs;
                tsRow.CurrencyID = item.CurrencyID;
                tsRow.EGCManualTech = item.EGCManualTech;
                tsRow.EGCRegion = item.EGCRegion;
                tsRow.EGCTechnology = item.EGCTechnology;
                tsRow.FormerCurrencyID = item.FormerCurrencyID;
                tsRow.FuelType = item.FuelType;
                tsRow.Gas_NDC = item.Gas_NDC;
                tsRow.HHV = item.HHV;
                tsRow.Metric = item.Metric;
                tsRow.NDC = item.NDC;
                tsRow.NumUnitsAtSite = item.NumUnitsAtSite;
                tsRow.Oil_NDC = item.Oil_NDC;
                tsRow.PrecBagYN = item.PrecBagYN;
                tsRow.PricingHub = item.PricingHub;
                tsRow.Region = item.Region;
                tsRow.Regulated = item.Regulated;
                tsRow.ScrubbersYN = item.ScrubbersYN;
                tsRow.State = item.State;
                tsRow.UnitID = item.UnitID;
                tsRow.UnitLabel = item.UnitLabel;
                tsRow.UnitName = item.UnitName; 

                dcPW.TSorts.InsertOnSubmit(tsRow);
                //dcPW.SubmitChanges();

                // create the NERCFactors record
                BuildNERCFactorsRecord(newRefnum, item.Refnum.Trim(), dcPW);

                // create the NercTurbine records
                BuildNERCTurbineRecords(newRefnum, item.Refnum.Trim(), dcPW);
            }
        }
        /// <summary>
        /// -copy latest record (no P if possible) for EACH refnum in study
        /// -change the following fields
        /// 	Refnum		- change the last 2 digits to current yr
        /// </summary>
        /// <param name="yearPortion">either a 2 or 3 based on whether the old refnum has a "P" or not</param>
        /// <param name="currYr">Int32.Parse(Globals.CurrentYear)</param>
        /// <param name="siteid">new refnum</param>
        /// <param name="oldSiteid">old refnum</param>
        /// <param name="dcPW">PowerWork DataContext</param>
        private static void BuildNERCFactorsRecord(string newRefnum, string oldRefnum, PowerWorkDataContext dcPW)
        {
            IQueryable queryNERCFactors = Queries.GetNERCFactorsByRefnum(dcPW, oldRefnum);
            foreach (NERCFactor item in queryNERCFactors)
            {
                bool refnumExist = false;
                NERCFactor nfRow = new NERCFactor();

                // check to see if the refnum record already exist for new Refnum, if so skip
                IQueryable queryRefnum = Queries.GetNERCFactorsByRefnum(dcPW, newRefnum);
                foreach (NERCFactor itemRefnum in queryRefnum)
                {
                    refnumExist = true;
                    break;
                }
                if (refnumExist)
                    break;

                // fields that need to change from prior year
                nfRow.Refnum = newRefnum;

                // fields that need to change from prior year
                nfRow.ACTStarts = item.ACTStarts;
                nfRow.NDC = item.NDC;
                nfRow.NMC = item.NMC;

                dcPW.NERCFactors.InsertOnSubmit(nfRow);
                //dcPW.SubmitChanges();
            }
        }
        /// <summary>
        /// -copy latest record (no P if possible) for EACH refnum in study
        /// ******* this one needs to have all records for each yr created *******
        /// -change the following fields
        /// 	Refnum		- change the last 2 digits to current yr
        /// </summary>
        /// <param name="yearPortion">either a 2 or 3 based on whether the old refnum has a "P" or not</param>
        /// <param name="currYr">Int32.Parse(Globals.CurrentYear)</param>
        /// <param name="siteid">new refnum</param>
        /// <param name="oldSiteid">old refnum</param>
        /// <param name="dcPW">PowerWork DataContext</param>
        private static void BuildNERCTurbineRecords(string newRefnum, string oldRefnum, PowerWorkDataContext dcPW)
        {
            IQueryable queryNERCTurbine = Queries.GetNERCTurbineByRefnum(dcPW, oldRefnum);
            foreach (NERCTurbine item in queryNERCTurbine)
            {
                bool refnumExist = false;
                string utilityunitcode = item.UtilityUnitCode;
                NERCTurbine ntRow = new NERCTurbine();

                // check to see if the refnum record already exist for new Refnum, if so skip
                IQueryable queryRefnum = Queries.GetNERCTurbineByRefnumUtilityUnitCode(dcPW, newRefnum, utilityunitcode);
                foreach (NERCTurbine itemRefnum in queryRefnum)
                {
                    refnumExist = true;
                    break;
                }
                if (refnumExist)
                {
                    // unit exist already in database
                }
                else
                {
                    // fields that need to change from prior year
                    ntRow.Refnum = newRefnum;

                    // fields that need to change from prior year
                    ntRow.Util_Code = item.Util_Code;
                    ntRow.Unit_Code = item.Unit_Code;
                    ntRow.UtilityUnitCode = item.UtilityUnitCode;
                    ntRow.UnitAbbr = item.UnitAbbr;
                    ntRow.TurbineID = item.TurbineID;
                    ntRow.NMC = item.NMC;
                    ntRow.NDC = item.NDC;
                    ntRow.TurbineType = item.TurbineType;

                    dcPW.NERCTurbines.InsertOnSubmit(ntRow);
                    //dcPW.SubmitChanges();
                }
            }
        }
        /// <summary>
        /// Adds new records to the ValStat table for PwrConsole to display for Checklist items
        /// </summary>
        /// <param name="siteid">New SiteID</param>
        /// <param name="dcPW"></param>
        private static void BuildCheckListRecords(string siteid, PowerWorkDataContext dcPW)
        {
            // check to see if the refnum record already exist for new Refnum, if so skip
            int queryCheckList = Queries.GetCheckListItemsByRefnum(dcPW, siteid).Count();

            // do nothing if records already exist for refnum
            if (queryCheckList > 0)
                return;
            else
            {
                IQueryable queryMasterItems = Queries.GetCheckListMasterItems(dcPW);

                foreach (ValStatSource item in queryMasterItems)
                {
                    ValStat nRow = new ValStat();

                    // fields that need to change from prior year
                    nRow.Refnum = siteid;
                    nRow.IssueID = item.IssueID;
                    nRow.IssueTitle = item.IssueTitle;
                    nRow.IssueText = item.IssueText;
                    nRow.PostedBy = item.PostedBy;
                    nRow.PostedTime = DateTime.Now;
                    nRow.Completed = item.Completed;

                    dcPW.ValStats.InsertOnSubmit(nRow);
                    dcPW.SubmitChanges();
                }
            }
        }

        #endregion Methods

    }
}
