using System;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

using ComUIUtilsAlias = Common.UI;
using DBUtilsAlias = Common.Database;

namespace PowerCPA
{
    internal partial class Globals
    {
        #region enums

        #endregion enums

        #region EventHandlers

        #endregion EventHandlers

        #region Help Provider

        #endregion Help Provider

        #region Constants
        #region Filenames
        #endregion Filenames

        #region Tablenames

        #endregion Tablenames


        #region encryption keys
        #endregion encryption keys

        #endregion Constants

        #region Global Variables

        public static string UserID = string.Empty;
        public static string Password = string.Empty;
        public static string UserIDDB = string.Empty;
        public static string PasswordDB = string.Empty;
        public static string DataSource = string.Empty;

        #endregion Global Variables

        #region Local Variables

        #endregion Local Variables

        #region Public Properties

        #endregion Public Properties

        #region Constructors
        #endregion Constructors

        #region Events Raised

        #endregion Events Raised

        #region Public Methods
        public static bool PromptForSQLLogin()
        {
            //SqlConnectionStringBuilder _connSB = new SqlConnectionStringBuilder();
            //// prompt for Power connection information
            //using (ComUIUtilsAlias.Security.SQLLogonForm dlg = new ComUIUtilsAlias.Security.SQLLogonForm())
            //{
            //    dlg.SqlConnectionBuilder = _connSB;
            //    dlg.EnableWorkstationID = false;
            //    DialogResult result = dlg.ShowDialog();
            //    if (result == DialogResult.OK)
            //    {
            //        Globals.CurrentUser = dlg.userID.ToUpper();
                    //if (!ValidateUser(dlg.userID, dlg.Password))
                    //{
                    //    MessageBox.Show(String.Format("User [{0}] not authorized to use application", dlg.userID));
                    //    return false;
                    //}
                    //else
                    //    return true;
                //}
                //else
                //{
                //    MessageBox.Show("User cancelled SQL connection");
                //    return false;
                //}
            //}
            return true;
        }
        public static SqlConnectionStringBuilder InitializeSQLConnectionStringBuilder(string _DbName, int _connectTimeout)
        {
            SqlConnectionStringBuilder _connSB = new SqlConnectionStringBuilder { 
                UserID = Globals.UserID, 
                Password = Globals.Password, 
                DataSource = Globals.DataSource, 
                ApplicationName = Application.ProductName, 
                InitialCatalog = _DbName, 
                ConnectTimeout = _connectTimeout };
            return _connSB;
        }

        #endregion Public Methods

        #region Private Methods
        private static bool ValidateUser(string _userid, string _password)
        {
            int result = 0;
            string sqlSelect = String.Format("SELECT FirstName, LastName FROM CPAUsers WHERE UserID='{0}'", _userid);
            SqlConnectionStringBuilder _connSB = new SqlConnectionStringBuilder { 
                DataSource = Globals.DataSource, 
                ApplicationName = Application.ProductName, 
                InitialCatalog = "PowerGlobal", 
                UserID = _userid, 
                Password = _password };

            try
            {
                DataTable dt = DBUtilsAlias.DatabaseAccess.GetDataTable(_connSB, sqlSelect, "temp");
                if (dt.Rows.Count > 0)
                    result = 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Problem connecting to database. {0}", ex.Message));
            }
            return result > 0 ? true : false;
        }

        #endregion Private Methods

    }
}
